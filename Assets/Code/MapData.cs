﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public sealed class MapData 
{
	public List<int> spawns = new List<int>();

	public int startTime;
	public bool timeStopped;
}
