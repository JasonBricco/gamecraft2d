﻿using UnityEngine;
using UnityEngine.Events;

public class CameraControl : MonoBehaviour, IUpdatable
{
	[SerializeField] private Transform player;

	private float minX, maxX, minY, maxY;

	private void Awake()
	{
		Updater.Register(this);

		float vertical = Camera.main.orthographicSize;
		float horizontal = vertical * Screen.width / Screen.height;

		minX = horizontal - 0.5f;
		maxX = Map.Width - horizontal - 0.5f;;
		minY = vertical - 0.5f;
		maxY = Map.Height - vertical - 0.5f;
	}

	public void UpdateTick()
	{
		GameState state = StateManager.CurrentState;

		if (state == GameState.Editing)
		{
			float x = Input.GetAxis("Horizontal");
			float y = Input.GetAxis("Vertical");

			transform.Translate(new Vector3(x, y, 0) * 10.0f * Time.deltaTime);

			Vector3 pos = transform.position;
			pos.x = Mathf.Clamp(pos.x, minX, maxX);
			pos.y = Mathf.Clamp(pos.y, minY, maxY);
			transform.position = pos;
		}
		else if (state == GameState.Playing)
		{
			Vector3 camPos = transform.position;
			camPos.x = player.position.x;
			camPos.y = Mathf.Max(player.position.y, minY);
			transform.position = camPos;
		}
	}
}
