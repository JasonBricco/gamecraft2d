﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;

public enum GameEvent
{
	Initialize,
	SettingSpawns,
	DoneSettingSpawns,
	SaveData
}

public sealed class Map : MonoBehaviour, IUpdatable
{
	public const int Width = 512, Height = 512;

	private static Chunk[,] chunks = new Chunk[32, 32];
	private static MapData mapData = new MapData();

	public delegate void GameEventDelegate(GameEvent type);
	public static event GameEventDelegate OnGameEvent;

	private void Awake()
	{
		Updater.Register(this);
		StateManager.ChangeState(GameState.MainMenu);
	}

	private void Start()
	{
		for (int x = 0; x < chunks.GetLength(0); x++)
		{
			for (int y = 0; y < chunks.GetLength(1); y++)
				chunks[x, y] = new Chunk(new Vector2i(x, y));
		}
	}

	public static void SendGameEvent(GameEvent type)
	{
		if (OnGameEvent != null)
			OnGameEvent(type);
	}

	public void UpdateTick()
	{
		for (int x = 0; x < chunks.GetLength(0); x++)
		{
			for (int y = 0; y < chunks.GetLength(1); y++)
				chunks[x, y].DrawMeshes();
		}
	}

	public static MapData MapData
	{
		get { return mapData; }
	}

	public static Chunk GetChunk(int x, int y)
	{
		return chunks[x >> Chunk.SizeBits, y >> Chunk.SizeBits];
	}

	public static ushort GetBlock(int x, int y)
	{
		return GetChunk(x, y).GetBlock(x & Chunk.Size - 1, y & Chunk.Size - 1);
	}

	public static void SetBlock(int x, int y, ushort block)
	{
		GetChunk(x, y).SetBlock(x & Chunk.Size - 1, y & Chunk.Size - 1, block);
	}

	public static ushort GetBlockSafe(int x, int y)
	{
		if (IsInMap(x, y))
			return GetChunk(x, y).GetBlock(x & Chunk.Size - 1, y & Chunk.Size - 1);

		return BlockType.Air;
	}

	public static void SetBlockSafe(int x, int y, ushort block)
	{
		if (IsInMap(x, y))
			 GetChunk(x, y).SetBlock(x & Chunk.Size - 1, y & Chunk.Size - 1, block);
	}

	public static Chunk SetBlockAndGetChunk(int x, int y, ushort block)
	{
		Chunk chunk = GetChunk(x, y);
		chunk.SetBlock(x & Chunk.Size - 1, y & Chunk.Size - 1, block);
		return chunk;
	}

	public static byte GetLight(int x, int y)
	{
		return GetChunk(x, y).light[x & Chunk.Size - 1, y & Chunk.Size - 1];
	}

	public static byte GetLightSafe(int x, int y)
	{
		if (IsInMap(x, y))
			return GetChunk(x, y).light[x & Chunk.Size - 1, y & Chunk.Size - 1];

		return LightUtils.MinLight;
	}

	public static void SetLight(int x, int y, byte light)
	{
		GetChunk(x, y).light[x & Chunk.Size - 1, y & Chunk.Size - 1] = light;
	}

	public static bool IsInMap(int x, int y)
	{
		if (x >= 0 && y >= 0 && x < Width && y < Height)
			return true;

		return false;
	}

	public static bool IsInChunk(int x, int y)
	{
		if (x >= 0 && y >= 0 && x < Chunk.Size && y < Chunk.Size)
			return true;

		return false;
	}

	private void OnApplicationQuit()
	{
		if (StateManager.CurrentState != GameState.MainMenu)
		{
			SendGameEvent(GameEvent.SaveData);
			SaveMap();
		}
	}

	private void SaveMap()
	{
		for (int x = 0; x < chunks.GetLength(0); x++)
		{
			for (int y = 0; y < chunks.GetLength(1); y++)
				chunks[x, y].Save();
		}
			
		StreamWriter dataWriter = new StreamWriter(Application.persistentDataPath + "/Data.txt");

		string json = JsonUtility.ToJson(mapData);
		dataWriter.Write(json);
		dataWriter.Close();
	}

	public static void LoadMap()
	{
		for (int x = 0; x < chunks.GetLength(0); x++)
		{
			for (int y = 0; y < chunks.GetLength(1); y++)
				chunks[x, y].Load();
		}

		string path = Application.persistentDataPath + "/Data.txt";

		if (File.Exists(path))
		{
			StreamReader reader = new StreamReader(path);
			string json = reader.ReadToEnd();
			mapData = JsonUtility.FromJson<MapData>(json);
			reader.Close();
		}
	}

	public static void DeleteMap()
	{
		string path = Application.persistentDataPath + "/Map.txt";

		if (File.Exists(path))
			File.Delete(path);
	}
}
