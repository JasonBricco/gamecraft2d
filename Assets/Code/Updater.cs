﻿using UnityEngine;
using System.Collections.Generic;

public class Updater : MonoBehaviour 
{
	private static List<IUpdatable> behaviors = new List<IUpdatable>();

	public static void Register(IUpdatable item)
	{
		behaviors.Add(item);
	}

	public void Update()
	{
		for (int i = 0; i < behaviors.Count; i++)
			behaviors[i].UpdateTick();
	}
}
