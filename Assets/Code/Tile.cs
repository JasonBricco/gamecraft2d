﻿using UnityEngine;

public struct Tile
{
	public ushort backBlock;
	public ushort mainBlock;
	public ushort frontBlock;

	public ushort this[int index]
	{
		get 
		{
			switch (index)
			{
			case 0:
				return backBlock;

			case 1:
				return mainBlock;

			case 2:
				return frontBlock;

			default:
				return mainBlock;
			}
		}
		set
		{
			switch (index)
			{
			case 0:
				backBlock = value;
				break;

			case 1:
				mainBlock = value;
				break;

			case 2:
				frontBlock = value;
				break;
			}
		}
	}
}
