﻿using UnityEngine;

public class CollisionManager 
{
	private BlockCollider[] colliders = new BlockCollider[12];
	private ushort[,] surrounding = new ushort[3, 4];

	public CollisionManager(GameObject collider)
	{
		GameObject collisionParent = new GameObject("Colliders");

		for (int i = 0; i < colliders.Length; i++)
		{
			GameObject colObj = GameObject.Instantiate(collider) as GameObject;
			colObj.transform.parent = collisionParent.transform;
			colliders[i] = colObj.GetComponent<BlockCollider>();
		}
	}

	public ushort GetSurroundingBlock(int x, int y)
	{
		return surrounding[x, y];
	}

	public void SetColliders(Vector3 pos)
	{
		Vector2i groundBlock = Utils.GetBlockPos(new Vector2(pos.x, pos.y - 1.0f));
		Vector2i blockPos;
		int index = 0;

		for (int y = 0; y < 4; y++)
		{
			for (int x = -1; x <= 1; x++)
			{
				blockPos = new Vector2i(groundBlock.x + x, groundBlock.y + y);
				ushort ID = Map.GetBlockSafe(blockPos.x, blockPos.y);
				surrounding[x + 1, y] = ID;
				BlockRegistry.GetBlock(ID).SetCollision(colliders[index], blockPos.x, blockPos.y);
				index++;
			}
		}
	}
}
