﻿using UnityEngine;

public enum CollisionType
{
	None,
	Box
}

public class BlockCollider : MonoBehaviour 
{
	public BoxCollider box;
	public CollisionType type = CollisionType.Box;

	public void EnableBox(float x, float y, float sizeX, float sizeY)
	{
		type = CollisionType.Box;
		transform.position = new Vector3(x, y);
		box.size = new Vector3(sizeX, sizeY, 1.0f);

		if (!box.enabled) box.enabled = true;
	}

	public void Disable()
	{
		if (box.enabled) box.enabled = false;
	}
}