﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public sealed class UIStore : MonoBehaviour 
{
	[SerializeField] private GameObject[] elements;
	private static Dictionary<string, GameObject> items = new Dictionary<string, GameObject>();

	private void Awake()
	{
		for (int i = 0; i < elements.Length; i++)
			items.Add(elements[i].name, elements[i]);
	}

	public static GameObject GetObject(string name)
	{
		return items[name];
	}

	public static T GetUI<T>(string name)
	{
		return items[name].GetComponent<T>();
	}
}