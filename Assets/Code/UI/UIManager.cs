﻿using UnityEngine;
using UnityEngine.UI;

public sealed class UIManager : MonoBehaviour 
{
	private GameObject mainMenuWindow;
	private GameObject blockWindow;
	private GameObject mapSettingsWindow;

	private GameObject playButton, doneButton, modeBarToggleButton;
	private Text currentText;
	private GameObject modeBar;
	private Toggle stopTimeToggle;

	private bool allowModeButtons = false, modeBarActive = true;

	private void Start()
	{
		mainMenuWindow = UIStore.GetObject("MainMenuWindow");
		blockWindow = UIStore.GetObject("BlockWindow");
		mapSettingsWindow = UIStore.GetObject("MapSettingsWindow");
		playButton = UIStore.GetObject("PlayButton");
		doneButton = UIStore.GetObject("DoneSpawningButton");
		currentText = UIStore.GetUI<Text>("CurrentBlock");
		modeBar = UIStore.GetObject("ModeBar");
		modeBarToggleButton = UIStore.GetObject("ModeBarToggleButton");
		stopTimeToggle = UIStore.GetUI<Toggle>("StopTimeToggle");

		mainMenuWindow.SetActive(true);
		blockWindow.SetActive(false);
		modeBar.SetActive(false);
		modeBarToggleButton.SetActive(false);

		Map.OnGameEvent += (type) => 
		{
			switch (type)
			{
			case GameEvent.Initialize:
				MapData data = Map.MapData;
				stopTimeToggle.isOn = data.timeStopped;
				break;

			case GameEvent.SettingSpawns:
				allowModeButtons = false;
				playButton.SetActive(false);
				doneButton.SetActive(true);
				break;

			case GameEvent.DoneSettingSpawns:
				allowModeButtons = true;
				playButton.SetActive(true);
				doneButton.SetActive(false);
				break;
			}
		};

		StateManager.OnStateChanged += (state) => 
		{ 
			switch (state)
			{
			case GameState.MainMenu:
				allowModeButtons = false;
				break;

			case GameState.Playing:
				allowModeButtons = false;
				modeBar.SetActive(false);
				modeBarToggleButton.SetActive(false);
				playButton.SetActive(false);
				mapSettingsWindow.SetActive(false);
				break;

			case GameState.Editing:
				allowModeButtons = true;
				playButton.SetActive(true);
				modeBar.SetActive(true);
				modeBarToggleButton.SetActive(true);
				break;
			}
		};
	}

	private void Update()
	{
		if (allowModeButtons)
		{
			if (Input.GetKeyDown(KeyCode.E))
			{
				if (!blockWindow.activeSelf)
				{
					blockWindow.SetActive(true);

					if (mapSettingsWindow.activeSelf)
						mapSettingsWindow.SetActive(false);
				}
				else
					blockWindow.SetActive(false);
			}
		}
	}

	public void StartButtonHandler()
	{
		mainMenuWindow.SetActive(false);
		MainMenu.StartGame();
	}

	public void PlayModeButtonHandler()
	{
		MapEditor.EnterPlayMode();
	}

	public void BlockSelectedButtonHandler(BlockID value)
	{
		MapEditor.SetActiveBlock(value);
		blockWindow.SetActive(false);
	}

	public void OnBlockHover(BlockID value)
	{
		ushort ID = (ushort)value.ID;
		currentText.text = BlockRegistry.GetBlock(ID).Name;
	}

	public void OnExitHover()
	{
		currentText.text = "";
	}

	public void ToggleModeBarButtonHandler(RectTransform arrow)
	{
		if (modeBarActive)
		{
			modeBar.SetActive(false);
			arrow.SetPosY(12.0f);
			arrow.SetScaleY(-1.0f);
			modeBarActive = false;
		}
		else
		{
			modeBar.SetActive(true);
			arrow.SetPosY(60.0f);
			arrow.SetScaleY(1.0f);
			modeBarActive = true;
		}
	}

	public void MapSettingsHandler()
	{
		if (allowModeButtons)
		{
			mapSettingsWindow.SetActive(true);

			if (blockWindow.activeSelf)
				blockWindow.SetActive(false);
		}
	}

	public void BlockModeHandler()
	{
		if (allowModeButtons)
			blockWindow.SetActive(true);
	}

	public void StructureModeHandler()
	{
	}

	public void SetSpawnsButtonHandler()
	{
		mapSettingsWindow.SetActive(false);
		SpawnManager.StartSettingSpawns();
	}

	public void DoneSpawningButtonHandler()
	{
		SpawnManager.StopSettingSpawns();
	}

	public void SetDayButtonHandler()
	{
		Weather.SetDay();
		mapSettingsWindow.SetActive(false);
	}

	public void SetNightButtonHandler()
	{
		Weather.SetNight();
		mapSettingsWindow.SetActive(false);
	}

	public void StopTimeHandler(bool value)
	{
		Weather.ToggleTime(value);
	}

	public void CloseMapSettingsHandler()
	{
		mapSettingsWindow.SetActive(false);
	}

	public void DeleteMapButtonHandler()
	{
		Map.DeleteMap();
	}

	public void QuitButtonHandler()
	{
		Application.Quit();
	}
}
