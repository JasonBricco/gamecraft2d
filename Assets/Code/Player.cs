﻿using UnityEngine;

public sealed class Player : MonoBehaviour, IUpdatable
{
	[SerializeField] private GameObject blockCollider;
	private CollisionManager collision;

	private CharacterController controller; 

	private float gravity = 5.0f;
	private float speed = 5.0f;
	private float jumpSpeed = 0.0f;
	private float gravModifier = 1.0f;
	private bool facingRight = true;

	private void Awake()
	{
		Updater.Register(this);

		collision = new CollisionManager(blockCollider);
		controller = GetComponent<CharacterController>();
		gameObject.SetActive(false);

		StateManager.OnStateChanged += (state) => 
		{
			switch (state)
			{
			case GameState.Playing:
				Vector3 spawnPos = SpawnManager.GetSpawnPosition();
				spawnPos.y += 0.5f;
				transform.position = spawnPos;
				gameObject.SetActive(true);
				break;

			case GameState.Editing:
				gameObject.SetActive(false);
				break;
			}
		};
	}

	public void UpdateTick()
	{
		if (!gameObject.activeSelf) return;

		Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0.0f, 0.0f);

		if (move.x > 0 && !facingRight) Flip();
		if (move.x < 0 && facingRight) Flip();

		move *= speed * Time.deltaTime;

		jumpSpeed -= gravity * gravModifier * Time.deltaTime;
		jumpSpeed = Mathf.Max(jumpSpeed, 0);
		move.y += jumpSpeed * Time.deltaTime;

		gravModifier += 1.0f * Time.deltaTime;
		gravModifier = Mathf.Min(gravModifier, 10.0f);

		if (controller.isGrounded)
		{
			jumpSpeed = 0;
			gravModifier = 1.00f;

			if (Input.GetKey(KeyCode.Space))
				jumpSpeed = 10.3f;
		}

		move.y -= gravity * gravModifier * Time.deltaTime;
		Move(move);
	}

	private void FixedUpdate()
	{
		collision.SetColliders(transform.position);
	}

	private void Move(Vector3 moveVector)
	{
		moveVector = transform.TransformDirection(moveVector);
		controller.Move(moveVector);

		Vector3 playerPos = transform.position;

		if (playerPos.y <= 0)
			transform.position = new Vector3(playerPos.x, 515.0f, 0.0f);
	}

	private void Flip()
	{
		facingRight = !facingRight;

		Vector3 scale = transform.localScale;
		scale.x *= -1;
		transform.localScale = scale;
	}
}
