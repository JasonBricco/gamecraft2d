using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class VectorObject3D : MonoBehaviour 
{
	bool m_updateVerts = true;
	bool m_updateUVs = true;
	bool m_updateColors = true;
	bool m_updateTris = true;
	Mesh m_mesh;
	VectorLine m_vectorLine;
			
	public void SetVectorLine (VectorLine vectorLine, Material mat) {
		gameObject.AddComponent<MeshRenderer>();
		gameObject.AddComponent<MeshFilter>();
		m_vectorLine = vectorLine;
		GetComponent<MeshRenderer>().material = mat;
		SetupMesh();
	}
	
	public void Enable (bool enable) {
		if (this == null) return;	// Prevent random null ref error when stopping play in editor
		GetComponent<MeshRenderer>().enabled = enable;
	}
	
	public void SetTexture (Texture tex) {
		GetComponent<MeshRenderer>().material.mainTexture = tex;
	}
	
	public void SetMaterial (Material mat) {
		GetComponent<MeshRenderer>().material = mat;
	}
	
	void SetupMesh () {
		m_mesh = new Mesh();
		m_mesh.name = m_vectorLine.LineName;
		m_mesh.hideFlags = HideFlags.HideAndDontSave;
		GetComponent<MeshFilter>().mesh = m_mesh;
	}
	
	void LateUpdate () {
		if (m_updateVerts) {
			SetVerts();
		}
		if (m_updateUVs) {
			m_mesh.uv = m_vectorLine.UVs;
			m_updateUVs = false;
		}
		if (m_updateColors) {
			m_mesh.colors32 = m_vectorLine.LineColors;
			m_updateColors = false;
		}
		if (m_updateTris) {
			m_mesh.SetTriangles (m_vectorLine.Triangles, 0);
			m_updateTris = false;
		}
	}
	
	void SetVerts() {
		m_mesh.vertices = m_vectorLine.Vertices;
		m_updateVerts = false;
		m_mesh.RecalculateBounds();
	}
	
	public void SetName (string name) {
		if (m_mesh == null) return;
		m_mesh.name = name;
	}
	
	public void UpdateVerts () {
		m_updateVerts = true;
	}
	
	public void UpdateUVs () {
		m_updateUVs = true;
	}
	
	public void UpdateColors () {
		m_updateColors = true;
	}
	
	public void UpdateTris () {
		m_updateTris = true;
	}
	
	public void UpdateMeshAttributes () {
		m_mesh.Clear();
		m_updateVerts = true;
		m_updateUVs = true;
		m_updateColors = true;
		m_updateTris = true;
	}
	
	public void ClearMesh () {
		if (m_mesh == null) return;
		m_mesh.Clear();
	}
	
	public int VertexCount () {
		return m_mesh.vertexCount;
	}
}
	