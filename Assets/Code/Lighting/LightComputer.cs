﻿using UnityEngine;
using System.Collections.Generic;

public class LightComputer 
{
	public static void Recompute(Vector2i pos, Queue<Vector2i> nodes, List<Chunk> chunks) 
	{
		byte oldLight = Map.GetLight(pos.x, pos.y);
		byte light = BlockRegistry.GetBlock(Map.GetBlock(pos.x, pos.y)).LightEmitted;

		if (oldLight > light)
			Remove(pos, nodes, chunks);

		if (light > LightUtils.MinLight)
		{
			Map.SetLight(pos.x, pos.y, light);
			nodes.Enqueue(pos);
			ScatterNodes(nodes, chunks);
		}
		else 
			Update(pos, nodes, chunks);
	}

	private static void Remove(Vector2i pos, Queue<Vector2i> nodes, List<Chunk> chunks) 
	{
		Map.SetLight(pos.x, pos.y, LightUtils.MaxLight);
		nodes.Enqueue(pos);
		RemoveNodes(nodes, chunks);
	}

	private static void Update(Vector2i pos, Queue<Vector2i> nodes, List<Chunk> chunks) 
	{
		for (int i = 0; i < 4; i++)
		{
			Vector2i next = pos + Vector2i.directions[i];

			if (Map.IsInMap(next.x, next.y))
				nodes.Enqueue(next);
		}

		ScatterNodes(nodes, chunks);
	}

	public static void ScatterNodes(Queue<Vector2i> nodes, List<Chunk> chunks) 
	{
		while (nodes.Count > 0)
		{
			Vector2i pos = nodes.Dequeue();

			ushort ID = Map.GetBlock(pos.x, pos.y);
			int light = Map.GetLight(pos.x, pos.y) - 1;

			if (light <= LightUtils.MinLight) continue;

			for (int i = 0; i < 4; i++)
			{
				Vector2i nextPos = pos + Vector2i.directions[i];

				if (Map.IsInMap(nextPos.x, nextPos.y))
				{
					ID = Map.GetBlock(nextPos.x, nextPos.y);

					if (BlockRegistry.GetBlock(ID).Transparent && SetMax((byte)light, nextPos.x, nextPos.y))
						nodes.Enqueue(nextPos);

					Chunk chunk = Map.GetChunk(nextPos.x, nextPos.y);

					if (!chunk.flaggedForUpdate)
					{
						chunk.flaggedForUpdate = true;
						chunks.Add(chunk);
					}
				}
			}
		}
	}

	private static void RemoveNodes(Queue<Vector2i> nodes, List<Chunk> chunks) 
	{
		Queue<Vector2i> newLights = new Queue<Vector2i>();

		while (nodes.Count > 0)
		{
			Vector2i pos = nodes.Dequeue();

			int light = Map.GetLight(pos.x, pos.y) - 1;
			Map.SetLight(pos.x, pos.y, LightUtils.MinLight);

			if (light <= LightUtils.MinLight) continue;

			for (int i = 0; i < 4; i++)
			{
				Vector2i nextPos = pos + Vector2i.directions[i];

				if (Map.IsInMap(nextPos.x, nextPos.y))
				{
					ushort ID = Map.GetBlock(nextPos.x, nextPos.y);

					if (BlockRegistry.GetBlock(ID).Transparent)
					{
						if (Map.GetLight(nextPos.x, nextPos.y) <= light)
							nodes.Enqueue(nextPos);
						else
							newLights.Enqueue(nextPos);
					}

					if (BlockRegistry.GetBlock(ID).LightEmitted > LightUtils.MinLight)
						newLights.Enqueue(nextPos);

					Chunk chunk = Map.GetChunk(nextPos.x, nextPos.y);

					if (!chunk.flaggedForUpdate)
					{
						chunk.flaggedForUpdate = true;
						chunks.Add(chunk);
					}
				}
			}	
		}

		ScatterNodes(newLights, chunks);
	}

	public static bool SetMax(byte light, int x, int y) 
	{
		byte oldLight = Map.GetLight(x, y);

		if (oldLight < light)
		{
			Map.SetLight(x, y, light);
			return true;
		}

		return false;
	}
}
