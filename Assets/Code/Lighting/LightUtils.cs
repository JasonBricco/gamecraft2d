﻿using UnityEngine;

public static class LightUtils
{
	public const byte MaxLight = 10;
	public const byte MinLight = 1;
	public const float Div3 = 1.0f / 3.0f;

	public static Color32 GetBlockLight(Vector2i pos) 
	{
		byte light = (byte)(Map.GetLightSafe(pos.x, pos.y) * 25);
		return new Color32(light, light, light, 245);
	}

	public static Color32 Average(Color32 first, Color32 second, Color32 third)
	{
		int r = (int)((first.r + second.r + third.r) * Div3);
		int b = (int)((first.b + second.b + third.b) * Div3);
		int g = (int)((first.g + second.g + third.g) * Div3);

		return new Color32((byte)r, (byte)b, (byte)g, 245);
	}

	public static Color32 Average(Color32 first, Color32 second)
	{
		int r = (first.r + second.r) >> 1;
		int b = (first.b + second.b) >> 1;
		int g = (first.g + second.g) >> 1;

		return new Color32((byte)r, (byte)b, (byte)g, 245);
	}
}
