﻿using UnityEngine;

public static class Utils 
{
	public static Vector2i GetBlockPos(Vector2 pos)
	{
		return new Vector2i(Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.y));
	}
}
