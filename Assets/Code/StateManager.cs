﻿using UnityEngine;

public enum GameState
{
	MainMenu,
	Editing,
	Playing
}

public static class StateManager
{
	public delegate void StateChangedEvent(GameState state);
	public static event StateChangedEvent OnStateChanged;

	private static GameState currentState;

	public static GameState CurrentState
	{
		get { return currentState; }
	}

	public static void ChangeState(GameState newState)
	{
		currentState = newState;

		if (OnStateChanged != null)
			OnStateChanged(newState);
	}
}
