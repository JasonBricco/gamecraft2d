﻿using UnityEngine;

public enum ShaderType
{
	Diffuse
}

public class Materials : MonoBehaviour
{
	private static Material[] materials = new Material[1];
	private static Shader[] shaders = new Shader[1];

	private void Awake()
	{
		materials[0] = (Material)Resources.Load("Materials/Standard");
		shaders[0] = (Shader)Resources.Load("Shaders/Diffuse");
	}

	public static Material GetMaterial(int meshIndex)
	{
		return materials[meshIndex];
	}

	public static Shader GetShader(ShaderType type)
	{
		return shaders[(int)type];
	}
}