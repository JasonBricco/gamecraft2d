﻿using UnityEngine;

public sealed class MainMenu : MonoBehaviour 
{
	public static void StartGame()
	{
		Map.LoadMap();
		Map.SendGameEvent(GameEvent.Initialize);
		StateManager.ChangeState(GameState.Editing);
	}
}
