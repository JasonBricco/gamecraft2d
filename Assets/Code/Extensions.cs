﻿using UnityEngine;

public static class Extensions 
{
	public static void SetScaleY(this RectTransform rT, float value)
	{
		Vector3 scale = rT.localScale;
		scale.y = value;
		rT.localScale = scale;
	}

	public static void SetPosY(this RectTransform rT, float value)
	{
		Vector3 pos = rT.position;
		pos.y = value;
		rT.position = pos;
	}
}
