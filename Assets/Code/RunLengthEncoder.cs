﻿using UnityEngine;
using System.IO;

public class RunLengthEncoder 
{
	public static string Encode(Tile[] tiles) 
	{ 
		StringWriter writer = new StringWriter();

		int i = 0;
		int length = tiles.Length;

		for (int layer = 0; layer <= 2; layer++)
		{
			ushort currentCount = 0;
			ushort currentData = 0;

			for (i = 0; i < length; i++) 
			{
				Tile tile = tiles[i];
			
				ushort thisData = tile[layer];

				if (thisData != currentData) 
				{
					if (i != 0) 
					{
						writer.Write((char)currentCount);
						writer.Write((char)currentData);
					}

					currentCount = 1;
					currentData = thisData;
				}
				else
					currentCount++;

				if (i == length - 1) 
				{
					writer.Write((char)currentCount);
					writer.Write((char)currentData);
				}
			}
		}
			
		string compressedData = writer.ToString();
		writer.Close();

		return compressedData;
	}

	public static void Decode(string data, Tile[] tiles) 
	{
		StringReader reader = new StringReader(data);
		int length = Chunk.Size * Chunk.Size;

		for (int layer = 0; layer < 3; layer++)
		{
			int i = 0;

			while (i < length) 
			{
				int currentCount = reader.Read();

				ushort currentData = (ushort)reader.Read();
				int j = 0;

				while (j < currentCount) 
				{
					tiles[i][layer] = currentData;
					j++;
					i++;
				}
			}
		}

		reader.Close();
	}
}
