﻿using UnityEngine;
using UnityEngine.UI;

public sealed class Weather : MonoBehaviour, IUpdatable
{
	private const int SecondsPerMinute = 60;

	private int dayLength = 14;
	private float seconds = 0;
	private static int minutes = 0, startMinutes;
	private static bool stopTime = false;
	private static int day = 0, dusk = 5, night = 7, dawn = 12;

	private static Image background;

	private static bool forceFade = false;
	private static bool fadeWeather = false;
	private static float forceFadeTime = 1.0f;
	private static float forceLerpAmount = 0.0f;
	private static Color currentLight, targetLight;

	private GameObject rain;
	private bool isRaining = false;

	private static Color dayLight = new Color(0.96f, 0.96f, 0.96f, 1.0f);
	private static Color nightLight = new Color(0.05f, 0.05f, 0.05f, 1.0f);
	private static Color rainLight = new Color(0.4f, 0.4f, 0.4f, 1.0f);

	private void Start()
	{
		Updater.Register(this);

		startMinutes = day;
		background = UIStore.GetUI<Image>("Background");
		Light = dayLight;

		StateManager.OnStateChanged += (state) => 
		{
			switch (state)
			{
			case GameState.Editing:
				ForceFade(startMinutes == day ? dayLight : nightLight, 0.5f, startMinutes);
				break;
			}
		};

		Map.OnGameEvent += (type) => 
		{
			if (type == GameEvent.Initialize)
			{
				MapData data = Map.MapData;
				startMinutes = data.startTime;
				stopTime = data.timeStopped;
			}

			if (type == GameEvent.SaveData)
			{
				MapData data = Map.MapData;
				data.startTime = startMinutes;
				data.timeStopped = stopTime;
			}
		};
	}

	private static Color Light
	{
		get { return RenderSettings.ambientLight; }
		set 
		{ 
			RenderSettings.ambientLight = value; 
			background.color = value;
		}
	}

	public static void SetDay()
	{
		startMinutes = day;
		ForceFade(dayLight, 1.0f, day);
	}

	public static void SetNight()
	{
		startMinutes = night;
		ForceFade(nightLight, 1.0f, night);
	}

	public static void ToggleTime(bool stopped)
	{
		stopTime = stopped;
	}

	public void UpdateTick()
	{
		if (fadeWeather)
		{
			if (Light.r <= rainLight.r)
			{
				fadeWeather = false;
				return;
			}

			forceLerpAmount += Time.deltaTime / 4.0f;
			Light = Color.Lerp(Light, rainLight, forceLerpAmount);

			return;
		}

		if (forceFade)
		{
			forceLerpAmount += Time.deltaTime / forceFadeTime;
			ConstrainedLerp(currentLight, targetLight, forceLerpAmount);

			if (Light == targetLight)
				forceFade = false;

			return;
		}
			
		if (StateManager.CurrentState != GameState.Playing || stopTime) return;

		seconds += Time.deltaTime;

		if (seconds >= SecondsPerMinute)
		{
			minutes++;
			minutes = minutes % dayLength;
			seconds -= SecondsPerMinute;
		}

		if (minutes >= dusk && minutes < night)
		{
			float percent = GetTransitionPercent(dusk);
			ConstrainedLerp(dayLight, nightLight, percent);
		}

		if (minutes >= dawn)
		{
			float percent = GetTransitionPercent(dawn);
			ConstrainedLerp(nightLight, dayLight, percent);
		}
	}

	private float GetTransitionPercent(int transition)
	{
		float time = minutes + (seconds / SecondsPerMinute);
		return (time - transition) * 0.5f;
	}

	private void ConstrainedLerp(Color start, Color end, float lerpVal)
	{
		Color newColor = Color.Lerp(start, end, lerpVal);

		if (isRaining && newColor.r >= rainLight.r) return;

		Light = newColor;
	}

	private static void ForceFade(Color target, float time, int newMinutes)
	{
		currentLight = Light;
		targetLight = target;

		forceFadeTime = time;

		minutes = newMinutes != -1 ? newMinutes : minutes;
		forceLerpAmount = 0.0f;
		forceFade = true;
	}
}
