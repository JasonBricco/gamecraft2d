﻿using UnityEngine;
using UnityEngine.UI;

public enum DebugInfo
{
	ID,
	Light,
	Position
}

public class ShowBlockInfo : MonoBehaviour 
{
	public DebugInfo info;
	private Text label;

	private void Awake()
	{
		label = GetComponent<Text>();
	}

	private void Update()
	{
		bool editing = StateManager.CurrentState == GameState.Editing;

		Vector2i pos = MapEditor.GetCursorBlockPos();
		ushort ID = Map.GetBlockSafe(pos.x, pos.y);

		switch (info)
		{
		case DebugInfo.ID:
			label.text = editing ? "Block: " + ID + " (" + BlockRegistry.GetBlock(ID).Name + ")" : "Block: N/A";
			break;

		case DebugInfo.Light:
			label.text = editing ? "Light: " + Map.GetLightSafe(pos.x, pos.y) : "Light: N/A";
			break;

		case DebugInfo.Position:
			label.text = editing ? "Position: " + pos.x + ", " + pos.y : "Position: N/A";
			break;
		}
	}
}