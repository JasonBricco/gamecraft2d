﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public sealed class MapEditor : MonoBehaviour, IUpdatable
{
	public const float PlaceLimit = 0.03f;
	private float time = 0.0f;

	private static ushort activeBlock = BlockType.Grass;

	private UnityAction DoEdit;

	private Queue<Vector2i> lightNodes = new Queue<Vector2i>();
	private List<Chunk> chunksToUpdate = new List<Chunk>();

	public static void EnterPlayMode()
	{
		if (SpawnManager.CanSpawnPlayer())
			StateManager.ChangeState(GameState.Playing);
	}

	private void Awake()
	{
		Updater.Register(this);
		DoEdit = StandardEdit;

		Map.OnGameEvent += (type) => 
		{
			switch (type)
			{
			case GameEvent.SettingSpawns:
				DoEdit = SpawnEdit;
				break;

			case GameEvent.DoneSettingSpawns:
				DoEdit = StandardEdit;
				break;
			}
		};
	}

	public void UpdateTick()
	{
		if (StateManager.CurrentState == GameState.Editing)
			DoEdit();

		if (StateManager.CurrentState == GameState.Playing)
		{
			if (Input.GetKeyDown(KeyCode.Escape))
				StateManager.ChangeState(GameState.Editing);
		}
	}

	private void StandardEdit()
	{
		if (EventSystem.current.IsPointerOverGameObject())
			return;

		time += Time.deltaTime;

		if (Input.GetMouseButtonDown(0))
			SetBlock(false);

		if (Input.GetMouseButtonDown(1))
			SetBlock(true);

		if (Input.GetKey(KeyCode.LeftShift) && Input.GetMouseButton(0))
		{
			if (time >= PlaceLimit)
			{
				SetBlock(false);
				time -= PlaceLimit;
			}
		}

		if (Input.GetKey(KeyCode.LeftShift) && Input.GetMouseButton(1))
		{
			if (time >= PlaceLimit)
			{
				SetBlock(true);
				time -= PlaceLimit;
			}
		}
	}

	private void SpawnEdit()
	{
		if (EventSystem.current.IsPointerOverGameObject())
			return;
		
		if (Input.GetMouseButtonDown(0))
			SetSpawn();

		if (Input.GetMouseButtonDown(1))
			DeleteSpawn();
	}

	public static void SetActiveBlock(BlockID value)
	{
		activeBlock = (ushort)value.ID;
	}

	public static Vector2i GetCursorBlockPos()
	{
		Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		return new Vector2i(pos);
	}

	private void SetBlock(bool deleting)
	{
		ushort ID = deleting ? BlockType.Air : activeBlock;

		Vector2i blockPos = GetCursorBlockPos();

		if (BlockRegistry.GetBlock(ID).CanPlace(blockPos.x, blockPos.y))
		{
			Chunk chunk = Map.SetBlockAndGetChunk(blockPos.x, blockPos.y, ID);

			if (deleting)
				BlockRegistry.GetBlock(Map.GetBlockSafe(blockPos.x, blockPos.y)).OnDelete(blockPos.x, blockPos.y);
			else
				BlockRegistry.GetBlock(ID).OnPlace(blockPos.x, blockPos.y);

			chunk.flaggedForUpdate = true;
			chunk.modified = true;
			chunksToUpdate.Add(chunk);
			LightComputer.Recompute(blockPos, lightNodes, chunksToUpdate);

			for (int i = 0; i < chunksToUpdate.Count; i++)
				chunksToUpdate[i].BuildMesh();

			chunksToUpdate.Clear();
		}
	}

	private void SetSpawn()
	{
		if (SpawnManager.SpawnCount() < 8)
		{
			Vector2i pos = GetCursorBlockPos();

			if (Map.GetBlockSafe(pos.x, pos.y) == BlockType.Air && Map.GetBlockSafe(pos.x, pos.y + 1) == BlockType.Air)
			{
				Map.SetBlockSafe(pos.x, pos.y, BlockType.Blocker);
				Map.SetBlockSafe(pos.x, pos.y + 1, BlockType.Blocker);
				SpawnManager.SetSpawn(pos);
			}
		}
	}

	private void DeleteSpawn()
	{
		Vector2i pos = GetCursorBlockPos();

		if (SpawnManager.RemoveSpawn(pos))
		{
			Map.SetBlockSafe(pos.x, pos.y, BlockType.Air);
			Map.SetBlockSafe(pos.x, pos.y + 1, BlockType.Air);
		}
	}
}
