﻿using UnityEngine;

public class ScreenManager : MonoBehaviour, IUpdatable
{
	private int lastWidth, lastHeight;

	public delegate void ScreenChangedEvent();
	public static event ScreenChangedEvent OnScreenChanged;

	private void Awake()
	{
		Updater.Register(this);
		lastWidth = Screen.width;
		lastHeight = Screen.height;

		OnScreenChanged += () => 
		{
			lastWidth = Screen.width;
			lastHeight = Screen.height;
		};
	}

	private void SendScreenEvent()
	{
		if (OnScreenChanged != null)
			OnScreenChanged();
	}

	public void UpdateTick()
	{
		if (Screen.width != lastWidth)
		{
			int newHeight = (Screen.width >> 2) * 3;
			Screen.SetResolution(Screen.width, newHeight, false);
			SendScreenEvent();
		}
		else if (Screen.height != lastHeight)
		{
			int newWidth = (Screen.height / 3) * 4;
			Screen.SetResolution(newWidth, Screen.height, false);
			SendScreenEvent();
		}
	}
}
