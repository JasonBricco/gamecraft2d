﻿using UnityEngine;

public class BookshelfBlock : Block
{
	public BookshelfBlock()
	{
		name = "Bookshelf";
		textureCoords = new Vector2(3, 13);
	}
}
