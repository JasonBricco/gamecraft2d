﻿using UnityEngine;

public class IceBlock : Block 
{
	public IceBlock()
	{
		name = "Ice";
		textureCoords = new Vector2(3, 11);
	}
}
