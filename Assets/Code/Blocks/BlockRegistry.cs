﻿using UnityEngine;

public static class BlockRegistry 
{
	public static Block[] blocks = 
	{
		new AirBlock(),
		new StoneBlock(),
		new GrassBlock(),
		new DirtBlock(),
		new BedrockBlock(),
		new BloodstoneBlock(),
		new ClayBlock(),
		new BookshelfBlock(),
		new BrickBlock(),
		new IceBlock(),
		new ChestBlock(),
		new CoalOreBlock(),
		new CobaltOreBlock(),
		new ShockBlock(),
		new BlockerBlock(),
		new GlowstoneBlock()
	};

	public static Block GetBlock(int ID)
	{
		return blocks[ID];
	}
}
