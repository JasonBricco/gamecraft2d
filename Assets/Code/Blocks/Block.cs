﻿using UnityEngine;
using System.Collections.Generic;

public class Block
{
	public const float TileSize = 0.0625f;

	protected string name = "Unknown";
	protected Vector2 textureCoords = Vector2.zero;
	protected int meshIndex = 0;
	protected int buildID = BuilderType.Square;
	protected byte lightEmitted = LightUtils.MinLight;
	protected bool transparent = false;

	public virtual void Build(int x, int y, MeshData data, int wX, int wY)
	{
		BuildManager.Build(buildID, this, x, y, data, wX, wY);
	}

	public int MeshIndex
	{
		get { return meshIndex; }
	}

	public Vector2 TextureCoords
	{
		get { return textureCoords; }
	}

	public string Name
	{
		get { return name; }
	}

	public byte LightEmitted
	{
		get { return lightEmitted; }
	}

	public bool Transparent
	{
		get { return transparent; }
	}

	public virtual void OnPlace(int x, int y)
	{
	}

	public virtual bool CanPlace(int x, int y)
	{
		if (!Map.IsInMap(x, y)) return false;

		if (Map.GetBlock(x, y) == BlockType.Blocker)
			return false;

		return true;
	}

	public virtual void OnDelete(int x, int y)
	{
	}

	public virtual void SetCollision(BlockCollider col, int x, int y)
	{
		col.EnableBox(x, y, 1.0f, 1.0f);
	}
}
