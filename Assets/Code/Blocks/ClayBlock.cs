﻿using UnityEngine;

public class ClayBlock : Block
{
	public ClayBlock()
	{
		name = "Clay";
		textureCoords = new Vector2(8, 11);
	}
}
