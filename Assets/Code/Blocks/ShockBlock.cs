﻿using UnityEngine;

public class ShockBlock : Block
{
	public ShockBlock()
	{
		name = "Shock";
		textureCoords = new Vector2(2, 1);
	}
}
