﻿using UnityEngine;

public class BedrockBlock : Block
{
	public BedrockBlock()
	{
		name = "Bedrock";
		textureCoords = new Vector2(1, 14);
	}
}
