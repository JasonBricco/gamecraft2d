﻿using UnityEngine;

public class BloodstoneBlock : Block 
{
	public BloodstoneBlock()
	{
		name = "Bloodstone";
		textureCoords = new Vector2(7, 9);
	}
}
