﻿using UnityEngine;

public class GlowstoneBlock : Block 
{
	public GlowstoneBlock()
	{
		name = "Glowstone";
		textureCoords = new Vector2(9, 9);
		lightEmitted = LightUtils.MaxLight;
	}
}
