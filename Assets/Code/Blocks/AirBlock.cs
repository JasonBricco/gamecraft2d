﻿using UnityEngine;
using System.Collections;

public class AirBlock : Block 
{
	public AirBlock()
	{
		name = "Air";
		transparent = true;
	}

	public override void Build(int x, int y, MeshData data, int wX, int wZ)
	{
	}

	public override void SetCollision(BlockCollider col, int x, int y)
	{
		col.Disable();
	}
}
