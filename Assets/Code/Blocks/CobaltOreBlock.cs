﻿using UnityEngine;

public class CobaltOreBlock : Block
{
	public CobaltOreBlock()
	{
		name = "Cobalt Ore";
		textureCoords = new Vector2(0, 5);
	}
}
