﻿using UnityEngine;

public class StoneBlock : Block
{
	public StoneBlock()
	{
		name = "Stone";
		textureCoords = new Vector2(1, 15);
	}
}
