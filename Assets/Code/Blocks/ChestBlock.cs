﻿using UnityEngine;

public class ChestBlock : Block 
{
	public ChestBlock()
	{
		name = "Chest";
		textureCoords = new Vector2(11, 14);
	}
}
