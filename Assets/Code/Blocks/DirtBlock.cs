﻿using UnityEngine;

public class DirtBlock : Block
{
	public DirtBlock()
	{
		name = "Dirt";
		textureCoords = new Vector2(2, 15);
	}
}
