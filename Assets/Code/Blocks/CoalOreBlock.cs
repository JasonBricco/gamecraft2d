﻿using UnityEngine;

public class CoalOreBlock : Block
{
	public CoalOreBlock()
	{
		name = "Coal Ore";
		textureCoords = new Vector2(2, 13);
	}
}
