﻿using UnityEngine;
using System.Collections;

public class BlockerBlock : Block
{
	public BlockerBlock()
	{
		name = "Blocker";
	}

	public override void Build(int x, int y, MeshData data, int wX, int wZ)
	{
	}

	public override void SetCollision(BlockCollider col, int x, int y)
	{
		col.Disable();
	}
}
