using UnityEngine;

public sealed class BlockType
{
	public const ushort Air = 0;
	public const ushort Stone = 1;
	public const ushort Grass = 2;
	public const ushort Dirt = 3;
	public const ushort Bedrock = 4;
	public const ushort Bloodstone = 5;
	public const ushort Clay = 6;
	public const ushort Bookshelf = 7;
	public const ushort Brick = 8;
	public const ushort Ice = 9;
	public const ushort Chest = 10;
	public const ushort CoalOre = 11;
	public const ushort CobaltOre = 12;
	public const ushort Shock = 13;
	public const ushort Blocker = 14;
	public const ushort Glowstone = 15;
}
