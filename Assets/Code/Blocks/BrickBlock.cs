﻿using UnityEngine;

public class BrickBlock : Block
{
	public BrickBlock()
	{
		name = "Brick";
		textureCoords = new Vector2(7, 15);
	}
}
