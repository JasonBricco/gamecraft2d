﻿using UnityEngine;

public class GrassBlock : Block
{
	public GrassBlock()
	{
		name = "Grass";
		textureCoords = new Vector2(3, 15);
	}
}
