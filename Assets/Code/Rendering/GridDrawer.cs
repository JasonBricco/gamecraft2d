﻿using UnityEngine;
using System.Collections.Generic;

public sealed class GridDrawer : MonoBehaviour
{
	private static VectorLine grid;
	private static List<Vector3> gridPoints = new List<Vector3>();

	private void Awake()
	{
		grid = new VectorLine("Grid", gridPoints, 1.0f);

		Map.OnGameEvent += (type) => 
		{
			if (type == GameEvent.Initialize)
				CreateGrid(); 
		};

		StateManager.OnStateChanged += (state) => 
		{ 
			if (state == GameState.Editing)
			{
				grid.Active = true;
				grid.Draw3D();
			}
			else
				grid.Active = false;
		};

		ScreenManager.OnScreenChanged += () => 
		{ 
			if (StateManager.CurrentState == GameState.Editing)
				grid.Draw3D(); 
		};
	}

	private void CreateGrid()
	{
		float width = Map.Width - 0.5f;
		float height = Map.Height - 0.5f;

		for (float x = -0.5f; x <= width; x++)
		{
			gridPoints.Add(new Vector3(x, -0.5f, 0));
			gridPoints.Add(new Vector3(x, height, 0));
		}

		for (float y = -0.5f; y <= height; y++)
		{
			gridPoints.Add(new Vector3(-0.5f, y, 0));
			gridPoints.Add(new Vector3(width, y, 0));
		}

		grid.Draw3D();
	}
}
