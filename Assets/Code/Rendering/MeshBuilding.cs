﻿using UnityEngine;

public sealed class BuilderType
{
	public const int Square = 0;
}

public abstract class MeshBuilder
{
	public abstract void Build(Block block, int x, int y, MeshData meshData, int wX, int wZ);
}

public sealed class BuildManager
{
	private static MeshBuilder[] builders =
	{
		new SquareBuilder()
	};

	public static void Build(int buildID, Block block, int x, int y, MeshData data, int wX, int wY)
	{
		builders[buildID].Build(block, x, y, data, wX, wY);
	}
}