﻿using UnityEngine;
using System.Collections.Generic;

public class SquareBuilder : MeshBuilder
{
	private readonly Vector3[] vertices =
	{
		new Vector3(0.5f, -0.5f, 0.0f),
		new Vector3(0.5f, 0.5f, 0.0f),
		new Vector3(-0.5f, 0.5f, 0.0f),
		new Vector3(-0.5f, -0.5f, 0.0f)
	};
		
	public override void Build(Block block, int x, int y, MeshData meshData, int wX, int wZ)
	{
		int index = block.MeshIndex;

		AddTriangles(index, meshData);

		for (int i = 0; i < vertices.Length; i++)
			meshData.AddVertex(index, vertices[i], x, y);

		BuildLight(index, block, new Vector2i(x, y), meshData, wX, wZ);
		AddUVs(block, index, meshData);
	}

	private static void BuildLight(int index, Block block, Vector2i pos, MeshData data, int wX, int wZ)
	{
		byte light = block.LightEmitted;

		if (light > LightUtils.MinLight)
		{
			byte lightColor = (byte)(light * 25);
			Color32 color = new Color32(lightColor, lightColor, lightColor, 245); 
			data.AddColors(index, color, color, color, color);
		}
		else
		{
			Vector2i vec = new Vector2i(wX, wZ);

			Color32 a = GetVertexLight(vec, 1, -1);
			Color32 b = GetVertexLight(vec, 1, 1);
			Color32 c = GetVertexLight(vec, -1, 1);
			Color32 d = GetVertexLight(vec, -1, -1);

			data.AddColors(index, a, b, c, d);
		}
	}

	private static void AddTriangles(int index, MeshData data)
	{
		List<int> triangles = data.GetTriangles(index);
		int offset = data.GetOffset(index);

		triangles.Add(offset + 2);
		triangles.Add(offset + 1);
		triangles.Add(offset + 0);

		triangles.Add(offset + 3);
		triangles.Add(offset + 2);
		triangles.Add(offset + 0);
	}

	private static void AddUVs(Block block, int index, MeshData data)
	{
		Vector2 tile = block.TextureCoords;

		data.AddUV(index, new Vector2(Block.TileSize * tile.x + Block.TileSize, Block.TileSize * tile.y));
		data.AddUV(index, new Vector2(Block.TileSize * tile.x + Block.TileSize, Block.TileSize * tile.y + Block.TileSize));
		data.AddUV(index, new Vector2(Block.TileSize * tile.x, Block.TileSize * tile.y + Block.TileSize));
		data.AddUV(index, new Vector2(Block.TileSize * tile.x, Block.TileSize * tile.y));
	}

	public static Color32 GetVertexLight(Vector2i pos, int dx, int dy)
	{
		Vector2i a = pos + new Vector2i(dx, 0);
		Vector2i b = pos + new Vector2i(0, dy);
		Vector2i c = pos + new Vector2i(dx, dy);

		Color32 c1 = LightUtils.GetBlockLight(a);
		Color32 c2 = LightUtils.GetBlockLight(b);
		Color32 c3 = LightUtils.GetBlockLight(c);

		return LightUtils.Average(c1, c2, c3);
	}
}
