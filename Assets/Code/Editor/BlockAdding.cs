﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.IO;

public static class BlockAdding
{
	private static GameObject blockWindow; 

	private static string[] names = 
	{
		"Air", "Stone", "Grass", "Dirt", "Bedrock", "Bloodstone", "Clay", "Bookshelf", "Brick",
		"Ice", "Chest", "CoalOre", "CobaltOre", "Shock", "Blocker", "Glowstone"
	};

	private static string newLine = System.Environment.NewLine;

	[MenuItem("Tools/Code/Fill Block Data")]
	private static void FillTypeConstants()
	{
		blockWindow = GameObject.FindWithTag("BlockWindow");

		WriteBlockIDs();
		WriteBlockInstances();
	}

	private static void WriteBlockIDs()
	{
		string path = Application.dataPath + "/Code/Blocks/BlockType.cs";
		StreamWriter writer = new StreamWriter(path);

		writer.WriteLine("using UnityEngine;" + newLine);
		writer.WriteLine("public sealed class BlockType");
		writer.WriteLine("{");

		SetIDsFromList(writer);

		writer.WriteLine("}");
		writer.Close();
	}

	private static void WriteBlockInstances()
	{
		string path = Application.dataPath + "/Code/Blocks/BlockInit.cs";
		StreamWriter writer = new StreamWriter(path);

		writer.WriteLine("using UnityEngine;");
		writer.WriteLine("using System.Collections.Generic;" + newLine);
		writer.WriteLine("public sealed class BlockInit : MonoBehaviour");
		writer.WriteLine("{");
		writer.WriteLine("\tprivate void Awake()");
		writer.WriteLine("\t{");

		SetInstancesFromList(writer);

		writer.WriteLine("\t}");
		writer.WriteLine("}");
		writer.Close();
	}

	private static void SetIDsFromList(StreamWriter writer)
	{
		int current = 0;

		for (int i = 0; i < names.Length; i++)
		{
			if (names[i][0] == '!')
			{
				string name = names[i].Remove(0, 1);

				for (int j = 1; j <= 5; j++)
					WriteID(writer, name + j, current++);

				SetInspectorID(name, current - 1);
			}
			else
			{
				if (names[i] != "Air")
					SetInspectorID(names[i], current);

				WriteID(writer, names[i], current++);
			}
		}
	}

	private static void SetInstancesFromList(StreamWriter writer)
	{
		for (int i = 0; i < names.Length; i++)
		{
			if (names[i][0] == '!')
			{
				string name = names[i].Remove(0, 1);

				for (int j = 1; j <= 5; j++)
					WriteInstance(writer, name);
			}
			else
				WriteInstance(writer, names[i]);
		}
	}

	private static void WriteID(StreamWriter writer, string type, int ID)
	{
		writer.WriteLine("\tpublic const ushort " + type + " = " + ID + ";");
	}

	private static void WriteInstance(StreamWriter writer, string type)
	{
		writer.WriteLine("\t\tBlock.blocks.Add(new " + type + "Block());");
	}

	private static void SetInspectorID(string name, int ID)
	{
		Transform t = blockWindow.transform.Find(name);

		if (t == null)
		{
			Debug.LogWarning(name + " could not be found. Ensure it exists and is properly named.");
			return;
		}

		t.gameObject.GetComponent<BlockID>().ID = ID;
	}
}