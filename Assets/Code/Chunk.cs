﻿using UnityEngine;
using System.IO;

public sealed class Chunk
{
	public const int SizeBits = 4;
	public const int Size = 1 << SizeBits;

	private static MeshData data = new MeshData();

	private Tile[] tiles = new Tile[Size * Size];
	public byte[,] light = new byte[Size, Size];

	private Vector2i position;
	private Vector3 worldPosition;

	private Mesh[] meshes = new Mesh[MeshData.MeshCount];

	public bool modified = false;
	public bool flaggedForUpdate = false;

	private string savePath;

	public Chunk(Vector2i position)
	{
		savePath = Application.persistentDataPath + "/" + position.x + position.y + ".txt";

		position = new Vector2i(position.x * Size, position.y * Size);
		worldPosition = new Vector3(position.x, position.y);

		for (int x = 0; x < light.GetLength(0); x++)
		{
			for (int y = 0; y < light.GetLength(1); y++)
				light[x, y] = LightUtils.MinLight;
		}
	}

	public ushort GetBlock(int x, int y)
	{
		return tiles[(y * Size) + x].mainBlock;
	}

	public void SetBlock(int x, int y, ushort block)
	{
		tiles[(y * Size) + x].mainBlock = block;
	}

	public void DrawMeshes()
	{
		for (int i = 0; i < meshes.Length; i++)
		{
			if (meshes[i] != null)
				Graphics.DrawMesh(meshes[i], worldPosition, Quaternion.identity, Materials.GetMaterial(i), 0);
		}
	}

	public void BuildMesh()
	{
		flaggedForUpdate = false;

		for (int x = 0; x < Size; x++)
		{
			for (int y = 0; y < Size; y++)
			{
				ushort ID = GetBlock(x, y);

				if (ID != BlockType.Air)
					BlockRegistry.GetBlock(ID).Build(x, y, data, position.x + x, position.y + y);
			}
		}

		for (int i = 0; i < MeshData.MeshCount; i++)
		{
			GameObject.Destroy(meshes[i]);
			meshes[i] = data.GetMesh(i);
		}

		data.Clear();
	}

	public void Save()
	{
		if (modified)
		{
			FileStream stream = new FileStream(savePath, FileMode.Create);
			StreamWriter writer = new StreamWriter(stream);
			string encodedBlocks = RunLengthEncoder.Encode(tiles);

			writer.Write(encodedBlocks);
			writer.Close();
		}
	}

	public void Load()
	{
		if (File.Exists(savePath))
		{
			StreamReader reader = new StreamReader(savePath);
			string encodedBlocks = reader.ReadToEnd();

			RunLengthEncoder.Decode(encodedBlocks, tiles);
			BuildMesh();

			reader.Close();
		}
	}
}
