﻿using UnityEngine;
using System.Collections.Generic;

public class SpawnManager : MonoBehaviour 
{
	[SerializeField] private GameObject spawnObject;
	private static GameObject spawn;
	private static Dictionary<Vector2i, GameObject> spawns = new Dictionary<Vector2i, GameObject>(8);

	private void Awake()
	{
		spawn = spawnObject;

		Map.OnGameEvent += (type) => 
		{
			if (type == GameEvent.Initialize)
			{
				MapData data = Map.MapData;
				for (int i = 0; i < data.spawns.Count; i++)
				{
					int x = data.spawns[i] & 511;
					int y = data.spawns[i] >> 9;

					GameObject currentSpawn = (GameObject)Instantiate(spawn);
					currentSpawn.transform.position = new Vector3(x, y);
					spawns.Add(new Vector2i(x, y), currentSpawn);
				}
			}
		};

		StateManager.OnStateChanged += (state) => 
		{
			switch (state)
			{
			case GameState.Playing:
				foreach (GameObject obj in spawns.Values)
					obj.SetActive(false);
				break;

			case GameState.Editing:
				foreach (GameObject obj in spawns.Values)
				{
					if (!obj.activeSelf)
						obj.SetActive(true);
				}
				break;
			}
		};
	}

	public static void StartSettingSpawns()
	{
		Map.SendGameEvent(GameEvent.SettingSpawns);
	}

	public static void StopSettingSpawns()
	{
		Map.SendGameEvent(GameEvent.DoneSettingSpawns);
	}

	public static int SpawnCount()
	{
		return spawns.Count;
	}

	public static void SetSpawn(Vector2i pos)
	{
		GameObject currentSpawn = (GameObject)Instantiate(spawn);
		currentSpawn.transform.position = new Vector3(pos.x, pos.y);
		spawns.Add(pos, currentSpawn);
		int compressed = (pos.y * Map.Width) + pos.x;
		Map.MapData.spawns.Add(compressed);
	}

	public static bool RemoveSpawn(Vector2i pos)
	{
		GameObject currentSpawn = spawns[pos];
		spawns.Remove(pos);
		Destroy(currentSpawn);

		int compressed = (pos.y * Map.Width) + pos.x;
		return Map.MapData.spawns.Remove(compressed);
	}

	public static bool CanSpawnPlayer()
	{
		return Map.MapData.spawns.Count > 0;
	}

	public static Vector3 GetSpawnPosition()
	{
		MapData mapData = Map.MapData;
		int count = mapData.spawns.Count;

		int num = Random.Range(0, count);
		int index = mapData.spawns[num];

		int x = index & 511;
		int y = index >> 9;

		return new Vector3(x, y);
	}
}
